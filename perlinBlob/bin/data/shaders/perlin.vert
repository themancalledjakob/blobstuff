/*
 2D Perlin-Noise in the vertex shader, based originally on
 vBomb.fx HLSL vertex noise shader, from the NVIDIA Shader Library.
 
 http://developer.download.nvidia.com/shaderlibrary/webpages/shader_library.html#vbomb
 
 Original Perlin function substituted for Stefan Gustavson's
 texture-lookup-based Perlin implementation.
 
 Quartz Composer setup
 toneburst 2009
 
 http://machinesdontcare.wordpress.com
 
 */

////////////////////////
//  2D Perlin Noise   //
////////////////////////

/*
 2D Perlin-Noise from example by Stefan Gustavson, found at
 
 http://staffwww.itn.liu.se/~stegu/simplexnoise/
 
 */

uniform sampler2D permTexture;          // Permutation texture
const float permTexUnit = 1.0/256.0;        // Perm texture texel-size
const float permTexUnitHalf = 0.5/256.0;    // Half perm texture texel-size

float fade(in float t) {
    return t*t*t*(t*(t*6.0-15.0)+10.0);
}

float pnoise2D(in vec2 p)
{
    // Integer part, scaled and offset for texture lookup
    vec2 pi = permTexUnit*floor(p) + permTexUnitHalf;
    // Fractional part for interpolation
    vec2 pf = fract(p);
    
    // Noise contribution from lower left corner
    vec2 grad00 = texture2D(permTexture, pi).rg * 4.0 - 1.0;
    float n00 = dot(grad00, pf);
    
    // Noise contribution from lower right corner
    vec2 grad10 = texture2D(permTexture, pi + vec2(permTexUnit, 0.0)).rg * 4.0 - 1.0;
    float n10 = dot(grad10, pf - vec2(1.0, 0.0));
    
    // Noise contribution from upper left corner
    vec2 grad01 = texture2D(permTexture, pi + vec2(0.0, permTexUnit)).rg * 4.0 - 1.0;
    float n01 = dot(grad01, pf - vec2(0.0, 1.0));
    
    // Noise contribution from upper right corner
    vec2 grad11 = texture2D(permTexture, pi + vec2(permTexUnit, permTexUnit)).rg * 4.0 - 1.0;
    float n11 = dot(grad11, pf - vec2(1.0, 1.0));
    
    // Blend contributions along x
    vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade(pf.x));
    
    // Blend contributions along y
    float n_xy = mix(n_x.x, n_x.y, fade(pf.y));
    
    // We're done, return the final noise value.
    return n_xy;
}

/////////////////////
// Sphere Function //
/////////////////////

const float PI = 3.14159265;
const float TWOPI = 6.28318531;
uniform float BaseRadius;

vec4 sphere(in float u, in float v) {
    u *= PI;
    v *= TWOPI;
    vec4 pSphere;
    pSphere.x = BaseRadius * cos(v) * sin(u);
    pSphere.y = BaseRadius * sin(v) * sin(u);
    pSphere.z = BaseRadius * cos(u);
    pSphere.w = 1.0;
    return pSphere;
}

///////////////////////////
// Apply 2D Perlin Noise //
///////////////////////////

uniform vec3 NoiseScale;    // Noise scale, 0.01 > 8
uniform float Sharpness;    // Displacement 'sharpness', 0.1 > 5
uniform float Displacement; // Displcement amount, 0 > 2
uniform float Speed;        // Displacement rate, 0.01 > 1
uniform float Timer;        // Feed incrementing value, infinite

vec4 perlinSphere(in float u, in float v) {
    vec4 sPoint = sphere(u, v);
    // The rest of this function is mainly from vBomb shader from NVIDIA Shader Library
    vec4 noisePos = vec4(NoiseScale.xyz,1.0) * (sPoint + (Speed * Timer));
    float noise = (pnoise2D(noisePos.xy) + 1.0) * 0.5;;
    float ni = pow(abs(noise),Sharpness) - 0.25;
    vec4 nn = vec4(normalize(sPoint.xyz),0.0);
    return (sPoint - (nn * (ni-0.5) * Displacement));
}

////////////////////////////////
// Calculate Position, Normal //
////////////////////////////////

const float grid = 0.01;    // Grid offset for normal-estimation
varying vec3 norm;          // Normal

vec4 posNorm(in float u, in float v) {
    // Vertex position
    vec4 vPosition = perlinSphere(u, v);
    // Estimate normal by 'neighbour' technique
    // with thanks to tonfilm
    vec3 tangent = (perlinSphere(u + grid, v) - vPosition).xyz;
    vec3 bitangent = (perlinSphere(u, v + grid) - vPosition).xyz;
    norm = gl_NormalMatrix * normalize(cross(tangent, bitangent));
    // Return vertex position
    return vPosition;
}

//////////////////////////
// Phong Directional VS //
//////////////////////////

// -- Lighting varyings (to Fragment Shader)
varying vec3 lightDir0, halfVector0;
varying vec4 diffuse0, ambient;

void phongDir_VS() {
    // Extract values from gl light parameters
    // and set varyings for Fragment Shader
    lightDir0 = normalize(vec3(gl_LightSource[0].position));
    halfVector0 = normalize(gl_LightSource[0].halfVector.xyz);
    diffuse0 = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    ambient =  gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
    ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
}

///////////////
// Main Loop //
///////////////

uniform vec2 PreScale, PreTranslate;    // Mesh pre-transform

void main()
{
    vec2 uv = gl_Vertex.xy;
    // Offset XY mesh coords to 0 > 1 range
    uv += 0.5;
    
    // Pre-scale and transform mesh
    uv *= PreScale;
    uv += PreTranslate;
    
    // Calculate new vertex position and normal
    vec4 spherePos = posNorm(uv[0], uv[1]);
    
    // Calculate lighting varyings to be passed to fragment shader
    phongDir_VS();
    
    // Transform new vertex position by modelview and projection matrices
    gl_Position = gl_ModelViewProjectionMatrix * spherePos;
    
    // Forward current texture coordinates after applying texture matrix
    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
}