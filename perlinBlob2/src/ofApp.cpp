#include "ofApp.h"


float sin01(float in){
    float out = sin(in)*0.5+0.5;
    return out;
}


GLUquadricObj *quadric;

//--------------------------------------------------------------
void ofApp::setup(){
    ofDisableArbTex();
    shader.load("shaders/displace.vert", "shaders/displace.frag");
    permTexture.loadImage("util_img/permtexture.png");
    world.loadImage("util_img/stips.jpg");
    increment = 0.0;
    
    light.enable();
    light.setAmbientColor(ofColor::white);
    light.setSpotConcentration(0.01);
    light.setDiffuseColor(ofColor::blue);
    light.setSpecularColor(ofColor::red);
    light.setPointLight();
    light.setPosition(0,0,1200);
    ofBackground(125);
    
    quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluQuadricNormals(quadric, GLU_SMOOTH);
    ofSetBackgroundAuto(false);
    
    fbo.allocate(ofGetWidth(), ofGetHeight());
    fbo.begin();
    ofBackground(0);
    fbo.end();
}

//--------------------------------------------------------------
void ofApp::update(){
    ofSetWindowTitle(ofToString(ofGetFrameRate()));

}

ofMesh makeGridMesh(float W, float H, float meshSize){
    ofMesh mesh;
    //Set up vertices
    for (int y=0; y<H; y++) {
        for (int x=0; x<W; x++) {
            mesh.addVertex(ofPoint((x - W/2) * meshSize, (y - H/2) * meshSize, 0 )); // adding texure coordinates allows us to bind textures to it later // --> this could be made into a function so that textures can be swapped / updated
            mesh.addTexCoord(ofPoint(x/W,y/H));//x * (videoWidth / W), y * (videoHeight / H)));
            mesh.addColor(ofColor(ofRandom(255.0), ofRandom(255.0), ofRandom(255.0)));
            mesh.addNormal(ofVec3f(ofRandom(1.0),ofRandom(1.0),ofRandom(1.0)));
        }
    }
    
    //Set up triangles' indices
    for (int y=0; y<H-1; y++) {
        for (int x=0; x<W-1; x++) {
            int i1 = x + W * y;
            int i2 = x+1 + W * y;
            int i3 = x + W * (y+1);
            int i4 = x+1 + W * (y+1);
            mesh.addTriangle( i1, i2, i3 );
            mesh.addTriangle( i2, i4, i3 );
        }
    }
    return mesh;
}

//--------------------------------------------------------------
void ofApp::draw(){
//    ofBackground(0);
    ofEnableAlphaBlending();
    ofPushStyle();
    ofSetColor(sin01(increment*0.01)*255,60);
    ofRect(0,0,ofGetWidth(),ofGetHeight());
    ofPopStyle();
    increment += 1.1;
    
    fbo.begin();
    ofBackground(0);
    /*
     
     uniform vec3 NoiseScale;    // Noise scale, 0.01 > 8
     uniform float Sharpness;    // Displacement 'sharpness', 0.1 > 5
     uniform float Displacement; // Displcement amount, 0 > 2
     uniform float Speed;        // Displacement rate, 0.01 > 1
     uniform float Timer;        // Feed incrementing value, infinite
     
     
     uniform vec2 PreScale, PreTranslate;    // Mesh pre-transform
     */
    
    shader.begin();
    shader.setUniformTexture("bumpmap", permTexture.getTextureReference(), 0);
    shader.setUniformTexture("colormap", world.getTextureReference(), 1);
    shader.setUniformTexture("permTexture", permTexture.getTextureReference(), 2);
    shader.setUniform1f("maxHeight", 1.0);
    shader.setUniform1f("Time", increment);
    
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    ofRotate(increment,sin01(increment*0.0067),sin01(increment*0.01),0);
    ofEnableDepthTest();
    gluSphere(quadric, 60 * sin01(increment*0.001) +150, 60 * sin01(increment*0.01) + 80, (60 * sin01(increment*0.01)) + 80);
    
    ofDrawBox(40 * sin01(increment*0.1) + 2);
    ofDrawIcoSphere(2);
    
    shader.end();
    
//    gluSphere(quadric, 150, 120 * sin01(increment*0.01) + 8, (120 * sin01(increment*0.01)) + 8);
    ofDisableDepthTest();
    ofDisableAlphaBlending();
    fbo.end();
    fbo.draw(0, 0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}