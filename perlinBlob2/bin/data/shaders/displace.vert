uniform sampler2D colormap;
uniform sampler2D bumpmap;
uniform sampler2D permTexture;          // Permutation texture
varying vec2  TexCoord;
uniform float maxHeight;
uniform float Time;

varying vec4 pc;

const float permTexUnit = 1.0/256.0;        // Perm texture texel-size
const float permTexUnitHalf = 0.5/256.0;    // Half perm texture texel-size

float fade(in float t) {
    return t*t*t*(t*(t*6.0-15.0)+10.0);
}

float pnoise2D(in vec2 p)
{
    // Integer part, scaled and offset for texture lookup
    vec2 pi = permTexUnit*floor(p) + permTexUnitHalf;
    // Fractional part for interpolation
    vec2 pf = fract(p);
    
    // Noise contribution from lower left corner
    vec2 grad00 = texture2D(permTexture, pi).rg * 4.0 - 1.0;
    float n00 = dot(grad00, pf);
    
    // Noise contribution from lower right corner
    vec2 grad10 = texture2D(permTexture, pi + vec2(permTexUnit, 0.0)).rg * 4.0 - 1.0;
    float n10 = dot(grad10, pf - vec2(1.0, 0.0));
    
    // Noise contribution from upper left corner
    vec2 grad01 = texture2D(permTexture, pi + vec2(0.0, permTexUnit)).rg * 4.0 - 1.0;
    float n01 = dot(grad01, pf - vec2(0.0, 1.0));
    
    // Noise contribution from upper right corner
    vec2 grad11 = texture2D(permTexture, pi + vec2(permTexUnit, permTexUnit)).rg * 4.0 - 1.0;
    float n11 = dot(grad11, pf - vec2(1.0, 1.0));
    
    // Blend contributions along x
    vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade(pf.x));
    
    // Blend contributions along y
    float n_xy = mix(n_x.x, n_x.y, fade(pf.y));
    
    // We're done, return the final noise value.
    return n_xy;
}


//////////////////////////
// Phong Directional VS //
//////////////////////////

// -- Lighting varyings (to Fragment Shader)
varying vec3 lightDir0, halfVector0;
varying vec4 diffuse0, ambient;

void phongDir_VS() {
    // Extract values from gl light parameters
    // and set varyings for Fragment Shader
    lightDir0 = normalize(vec3(gl_LightSource[0].position));
    halfVector0 = normalize(gl_LightSource[0].halfVector.xyz);
    diffuse0 = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    ambient =  gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
    ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
}

////////////////////////////////
// Calculate Position, Normal //
////////////////////////////////

varying vec3 norm;          // Normal




void main(void) {
    norm = gl_Normal;
    
    // Calculate lighting varyings to be passed to fragment shader
    phongDir_VS();
    
	TexCoord = gl_MultiTexCoord0.st;

	vec4 bumpColor = texture2D(bumpmap, TexCoord);
	float df1 = 0.30*bumpColor.r + 0.59*bumpColor.g + 0.11*bumpColor.b;
    float df = (pnoise2D(abs(TexCoord) * abs(sin(Time*0.01)))*10.0* abs(sin(Time*0.005))) + pnoise2D(TexCoord*sin(Time*0.01)*abs(sin(Time*0.001)*40.0)) * 0.8 + df1;
    vec4 displace = vec4(gl_Normal * df * maxHeight * 100.0, 0.0);
	vec4 newVertexPos = displace + gl_Vertex;
    
    gl_Position = gl_ModelViewProjectionMatrix * newVertexPos;
    pc = vec4(gl_Normal, 1.0) * 0.5 + 0.5 * vec4(length(displace)*0.04);
} 