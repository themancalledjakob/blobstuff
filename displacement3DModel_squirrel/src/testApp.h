#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

#include "ofx3DModelLoader.h"

class testApp : public ofBaseApp{
public:

	void setup();
	void update();
	void draw();
    void updateMidColor();
    
    ofColor midColor;

    ofx3DModelLoader squirrelModel;
    ofVboMesh squirrel;
    
	ofImage colormap,bumpmap;
	ofShader shader;
	GLUquadricObj *quadratic;
	
    // camera
    //ofCamera cam; // add mouse controls for camera movement
	ofVideoGrabber vidGrabber;
    
    //anti aliasing
    ofFbo fbo;
    ofFbo texFbo;
    ofFbo squirrelFbo;
};

#endif

