#include "testApp.h"


//--------------------------------------------------------------
void testApp::setup(){
	ofSetWindowTitle("Chladni_NN_08");
	ofSetFrameRate(60); // if vertical sync is off, we can go a bit fast... this caps the framerate at 60fps.
	easing=0.13;
	zoom = 1;
	detail = 2;
    fbo.allocate(200, 200);
    fbo.begin();
    ofBackground(255, 255, 255);
    fbo.end();
}

//--------------------------------------------------------------
void testApp::update(){
    deltaX=mouseX-fbo.getWidth()/2-ex;
    deltaY=mouseY-fbo.getHeight()/2-ey;
    deltaX*=easing;
    deltaY*=easing;
    ex+=deltaX;
    ey+=deltaY;
}

//--------------------------------------------------------------
void testApp::draw(){
    fbo.begin();
   glTranslated(fbo.getWidth()/2, fbo.getHeight()/2, 10);
    for(int x=-fbo.getWidth()/2; x<fbo.getWidth()/2; x++) {
		for(int y=-fbo.getHeight()/2; y<fbo.getHeight()/2; y++) {
           switch (fc(x,y)) {
           // case 0: ofSetColor(235,0,95);
			//break;
			case 1: ofSetColor(255,255,255);
			break;
			case 2: ofSetColor(255,0,0);
			break;
            case 3: ofSetColor(0,0,0);
            break;

		    }
			ofRect(x, y, 2, 2);
            }
        }

////Dibuixa Cursor negre i blanc en creueta doble
//    glPushMatrix();
//    glTranslated(-fbo.getWidth()/2, -fbo.getHeight()/2,0);
//ofSetColor(200,200,200);
//    ofLine(mouseX, mouseY-6, mouseX, mouseY+6);
//    ofLine(mouseX-5, mouseY, mouseX+6, mouseY);
//ofSetColor(85,85,85);
//    ofLine(mouseX-5, mouseY+1, mouseX+6, mouseY+1);
//    ofLine(mouseX+1, mouseY-6, mouseX+1, mouseY+6);
//    glPopMatrix();

    fbo.end();
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
            fbo.draw(i*fbo.getWidth(),j*fbo.getHeight());
    fbo.draw(0,0);
}



//----------------------------------------------------------------------------
int testApp::calcula(double x, double y, double xshift, double yshift, double m, double n, double zoom, double detail){
	double pi = 3.14159265;
	double rx = x/fbo.getWidth();
	double ry = y/fbo.getHeight();
	double rm = m * zoom;
	double rn = n * zoom;
    return int(detail * (cos(rm*pi*rx)*cos(rn*pi*ry) - cos(rn*pi*rx)*cos(rm*pi*ry)));
}

//----------------------------------------------------------------------------
int testApp::fc(int x, int y) {

	double m = ex/7.321, n = ey/7.257;
	float  xshift = fbo.getWidth() , yshift = fbo.getHeight();

	int z = calcula(x, y, xshift, yshift, m, n, zoom, detail);
z = z % 3;
	 if (z < 0) {
	     switch (z) {
			case -0: return(1);
			case -1: return(2);
			case -2: return(3);
		      }
	        }
	else {
	switch (z) {
			case 0: return(1);
			case 1: return(2);
			case 2: return(3);
		}
	}

}

//--------------------------------------------------------------
void testApp::keyPressed  (int key){

}

//--------------------------------------------------------------
void testApp::keyReleased  (int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
      if(button == 0){
        zoom++;
        detail+=2;
        } else   {
        zoom--;
        detail-=2;
        }

}

//--------------------------------------------------------------
void testApp::mouseReleased(){

}


