//////////////////////////      include the header file!
#include "testApp.h"


void putImagesInVec(string path, vector<ofImage> * imagess){
    //some path, may be absolute or relative to bin/data
    ofDirectory dir(path);
    //only show png files
    dir.allowExt("jpg");
    //populate the directory object
    dir.listDir();
    
    //go through and print out all the paths
    for(int i = 0; i < dir.numFiles(); i++){
        ofImage image;
        image.loadImage(path + dir.getName(i));
        imagess->push_back(image);
    }
}

//------------------------------------------------------------------u
//-----------------------------------------------------------------l
//----- S E T U P ------------------------------------------------t
//---------------------------------------------------------------r
//--------------------------------------------------------------a
void testApp::setup(){
    
//////////////////////////      set vertical sync true, or not. see what the difference is, if there is any
	ofSetVerticalSync(true);
    
//////////////////////////      disableArtTex(): enable, disable at the right moments! otherwise, blackness.
//////////////////////////      usually doesn't matter, but here it does
    
	ofDisableArbTex();
    
//////////////////////////      want to use alpha-stuff? enable it
    
	ofEnableAlphaBlending();
    
//////////////////////////      load our shaderfiles into the shader
    
    shader.load("shaders/displace.vert", "shaders/displace.frag");
    
//////////////////////////      vidGrabber aka webcam
//////////////////////////      initialize the video grabber
    
	vidGrabber.setVerbose(true);
	vidGrabber.initGrabber(1024,512);
    vidGrabber.update();
    
    //////////////////////////      load images, will be done differently in a second
    
    putImagesInVec("images/", &images);
    putImagesInVec("images/pineapples/", &pineapples);
    
	colormap = &images[0].getTextureReference();
    
	bumpmap = &vidGrabber.getTextureReference();
    
    
//////////////////////////      allocate the fbos, so they know how big they are
    
    mainFbo.allocate(ofGetWidth(), ofGetHeight());
    texFbo.allocate(ofGetWidth(), ofGetHeight());
    squirrelFbo.allocate(ofGetWidth(), ofGetHeight());
    
//////////////////////////      setup our math meshes
    
    torus.enableFlatColors();
    mobius.enableFlatColors();
    
    parabala.setup(-2, 2, -2, 2, .1, .1);
    torus.setup(0, 2*M_PI, 0,2*M_PI, .1, .1);
    mobius.setup(0, 2*M_PI, 0, 1, .1, .1);
    tube.setup(-2, 2, -2, 2,0,2,.1, .1);
    spriral.setup(0, 2*M_PI, .01);
    thing.setup(-10, 10, -10, 10, 1.0, 1.0);
    steiner.setup(-1, 1, -1, 1, .1, .1);
    boys.setup(-1, 1, -1, 1, .1, .1);
    breather.setup(-17, 17, -13, 13, 0.5, 0.5);
    breather.setA(abs(sin(ofGetSystemTimeMicros()*0.0000001)) * 0.4 + 0.7);
    breather.setUV(ofPoint(0.0,1.0));

//////////////////////////      ofSetBackgroundAuto(bool): if true, it clears the screen every frame
//////////////////////////      if false, it doesn't and you get this tracing effect we want
    
    ofSetBackgroundAuto(false);
    
//////////////////////////      clear the first frame of the fbo with a background, or ofClear(), doesn't matter
    
    squirrelFbo.begin();
    ofBackground(255,255,255,0);
    squirrelFbo.end();
    
//////////////////////////      clear the first frame of the fbo with a background, or ofClear(), doesn't matter
    mainFbo.begin();
    ofBackground(0);
    mainFbo.end();
    
//////////////////////////      clear the first frame of the screen with a background, or ofClear(), doesn't matter
	ofBackground(255,255,215);
    
}

//--------------------------------------------------------------s
//--------------------------------------------------------------u
//----- U P D A T E --------------------------------------------p
//--------------------------------------------------------------e
//--------------------------------------------------------------r
void testApp::update(){
    
//////////////////////////      tell us how fast we are in the window title
    
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
//////////////////////////      recalculate mathematical meshes
    thing.reload();
    steiner.reload();
    boys.reload();
    
    breather.setA(abs(sin(ofGetSystemTimeMicros()*0.0000001)) * 0.4 + 0.7);
    breather.reload();
    
//////////////////////////      make an ofVboMesh from a specific mesh
    mesh = thing.getMesh();
    
    
//////////////////////////      update webcam image
    vidGrabber.update();
    
    
//////////////////////////      stuff
    texFbo.begin();
    float sinuss =sin(ofGetSystemTimeMicros()*0.000001);
    ofBackground(sinuss*25 + 25);
    ofPushStyle();
    ofSetColor(ofColor::white);
    ofCircle(texFbo.getWidth()/2, texFbo.getHeight()/2, sinuss*texFbo.getHeight()/4+texFbo.getHeight()/4);
    ofPopStyle();
    texFbo.end();
    
    
//////////////////////////      more stuff
    
    squirrelFbo.begin();
    ofClear(0,0,0,0);
    ofDisableArbTex();
	shader.begin();
	shader.setUniformTexture("colormap", *colormap, 1);
	shader.setUniformTexture("bumpmap", *bumpmap, 2);
	shader.setUniformTexture("pineapples", pineapples[0].getTextureReference(), 3);
	shader.setUniform1f("maxHeight",1.7);
    int timeInt = ofGetSystemTimeMicros();
    float red = 0.5*sin(timeInt*0.0000001f)+0.5;
    float green = 0.5*sin(timeInt*0.000001f)+0.5;
    float blue = 0.5*cos(timeInt*0.00000011f)+0.5;
	shader.setUniform1f("red",red);
	shader.setUniform1f("green",green);
	shader.setUniform1f("blue",blue);
	shader.setUniform1f("dis",0.0);
    
    
//////////////////////////      other stuff
    glPushMatrix();
    float scale = ((float)mouseX/(float)ofGetWidth()) * 80.0;
    
    glTranslatef(ofGetWidth()/2,ofGetHeight()/2,0);
    
    glRotatef(-mouseY,1,0,0);
    glRotatef(mouseX+(timeInt%(360000000/2))*0.00005,0,1,0);
    glScalef(scale,scale,scale);
    glTranslatef(((mouseX/ofGetWidth()-0.5)*2.0), 0, 0);
//    ofEnableDepthTest();
//    mesh.draw();
//    ofDisableDepthTest();

//    ofVboMesh mesh2 = ofSpherePrimitive(2, 16).getMesh();
//
	shader.setUniform1f("dis",1.0);
//    steiner.getMesh().drawWireframe();
//    ofEnableDepthTest();
    //    mesh2.draw();
    breather.draw(true,true);
//    breather.getMesh().draw();
    boys.draw(true,true);
//    ofDisableDepthTest();
    
    glPopMatrix();
	shader.end();
    squirrelFbo.end();
    
    mainFbo.begin();
    ofEnableAlphaBlending();
    ofPushStyle();
    ofSetColor(0,40);
    ofRect(0,0,ofGetWidth(),ofGetHeight());
    ofPopStyle();
    ofPushMatrix();
    squirrelFbo.draw(0,0);
    ofPopMatrix();
    ofDisableAlphaBlending();
    mainFbo.end();
    
}

//--------------------------------------------------------------y
//---------------------------------------------------------------e
//----- D R A W --------------------------------------------------e
//-----------------------------------------------------------------a
//------------------------------------------------------------------h   
void testApp::draw(){
//    ofPushStyle();
////    ofSetColor(255,255,255,10);
////    colormap.draw(0,0,squirrelFbo.getWidth(),squirrelFbo.getHeight());
//    ofSetColor(255,0,255,10);
//    ofRect(0,0,ofGetWidth(),ofGetHeight());
    //    ofPopStyle();
    
//    glClear(GL_DEPTH_BUFFER_BIT);
    //    glClear(GL_COLOR_BUFFER_BIT);
    int timeInt = ofGetSystemTimeMicros();

    ofClear(0,0,0);
//    float scaler = 2.0;
//    ofTranslate(fbo.getWidth()/2, fbo.getHeight()/2);
//    ofPushMatrix();
//    ofRotate(-0.1*((int)(timeInt*0.00001)%3600),0,0,1);
//    ofTranslate(-scaler*fbo.getWidth()/2, -scaler*fbo.getHeight()/2);
//    fbo.draw(0,0,fbo.getWidth()*scaler,fbo.getHeight()*scaler);
    mainFbo.draw(0,0,ofGetWidth(),ofGetHeight());
//    
//    glPushMatrix();
//    float scale = ((float)mouseX/(float)ofGetWidth()) * 80.0;
//    
//    glTranslatef(ofGetWidth()/2,ofGetHeight()/2,0);
//    glRotatef(-mouseY,1,0,0);
//    glRotatef(mouseX+(timeInt%(360000000/2))*0.00005,0,1,0);
//    glScalef(scale,scale,scale);
//    glTranslatef(((mouseX/ofGetWidth()-0.5)*2.0), 0, 0);
//        
//    //draw in middle of the screen
//    //tumble according to mouse
//    //    glTranslatef(-ofGetWidth()/2/scale,-ofGetHeight()/2/scale,0);
//    
//    ofPushStyle();
//    ofEnableDepthTest();
////    mesh.setColorForIndices(0, mesh.getIndices().size(), ofColor::white);
//    ofSetColor(255);
//    //    mesh.draw();
//    mesh.enableColors();
//    mesh.setColorForIndices(0, mesh.getIndices().size(), ofColor(0));
//    ofSetLineWidth(1.0);
//    ofNoFill();
//    ofSetColor(255);
//    mesh.drawWireframe();
//    
//    ofDisableDepthTest();
//    ofPopStyle();
//    
//    glPopMatrix();
//    ofPopMatrix();
    
}