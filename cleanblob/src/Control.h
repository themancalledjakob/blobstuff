#pragma once

#include "ofMain.h"

#define MESH_DRAW_NOT 0
#define MESH_DRAW_WIREFRAME 1
#define MESH_DRAW_TEXTURED 2

#define MESH_TYPE_PRIMITIVE 0
#define MESH_TYPE_PARAMETRIC_SURFACE 1
#define MESH_TYPE_PARAMETRIC_CURVE 2
#define MESH_TYPE_3D_FUNCTION 3
#define MESH_TYPE_MODEL 2

struct ShaderControl {
    float maxHeight = -1.0;
    float dis = -1.0;
    float red = -1.0;
    float green = -1.0;
    float blue = -1.0;
    float c1minContrast = -1.0;
    float c1maxContrast = -1.0;
    float c1contrast = -1.0;
    float c2minContrast = -1.0;
    float c2maxContrast = -1.0;
    float c2contrast = -1.0;
    ofTexture * bumpmap;
    ofTexture * colormap1;
    ofTexture * colormap2;
};

struct PrimitiveControl {
    of3dPrimitive * primitive;
    float resolution = -1.0;
    ofVec3f scale = ofVec3f(-1.0,-1.0,-1.0);
    ofVec3f position = ofVec3f(-1.0,-1.0,-1.0);
    ofQuaternion rotation = ofQuaternion(0.0, 0.0, 0.0, 0.0);
    ShaderControl sc;
};

struct MeshControl {
    ofVboMesh * mesh;
    int draw = MESH_DRAW_NOT;
    int type = MESH_TYPE_PARAMETRIC_SURFACE;
    ofVec3f scale = ofVec3f(-1.0,-1.0,-1.0);
    ofVec3f position = ofVec3f(-1.0,-1.0,-1.0);
    ofQuaternion rotation = ofQuaternion(0.0, 0.0, 0.0, 0.0);
    ShaderControl sc;
};

class Control : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    void createMeshControl(ofVboMesh * mesh);
    void createPrimitiveControl(of3dPrimitive * mesh);
    
    vector<MeshControl> mc;

};
