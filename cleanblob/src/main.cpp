//////////////////////////      #includes in main.cpp: no-worry-yet stuff
#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"

/////super//comment///////      int main(): whaaat? the main function is an integer?????? but it makes sense actually.
/////super//comment///////      the integer is 0 if the program finishes nicely. no errors. no worries. zero problems. check?

    int main() {
        
//////////////////////////      hello window of the application
        
    ofAppGlutWindow window;
    
//////////////////////////      ofSetupOpenGL: define the window, aka set up openGL
//////////////////////////      numbers -> width, height
//////////////////////////      OF_WINDOW -> window
//////////////////////////      OF_FULLSCREEN -> fullscreen
        
    ofSetupOpenGL(&window, 1024,768, OF_WINDOW);			// <-------- setup the GL context
    
//////////////////////////      ofRunApp: run, app, run
        
	ofRunApp(new testApp());
        
}
