//..............................#ifndef#define: no-worry-yet stuff

#ifndef _TEST_APP
#define _TEST_APP

//////////////////////////      ofMain: you may want to include the main openFrameworks header-file. just a suggestion

#include "ofMain.h"

/////super//comment///////      always! include the header file of an addon that you want to use

//////////////////////////      ofx3DModelLoader: is an addon that comes with openFrameworks
//////////////////////////      it can load 3DModels. This is why we want it.

#include "ofx3DModelLoader.h"

//////////////////////////      MyEquations: make 3D Meshes based on mathematical equations, yeah!

#include "MyEquations.h"

//////////////////////////      Control: collect the things we want to control.

#include "Control.h"

/////super//comment///////      don't forget to say:" : public ofBaseApp{ " to get access to everything an oF-base-App can do


//////////////////////////      testApp: our main application class, welcome, testApp

class testApp : public ofBaseApp{
    
//////////////////////////      public: means, it's accessible by other classes. why not.
public:

/////super//comment///////      voids are things that do stuff. they don't return anything.
/////super//comment///////      other things give you something back, like: int gives you a number / string a word / whatever etc.
/////super//comment///////      voids don't do that. voids don't give. voids only do.
    
//////////////////////////      some standard oF-voids.
    
//////////////////////////      setup(): runs once at the beginning
    
	void setup();
    
//////////////////////////      update(): runs until the application stops as fast as it can
    
	void update();
    
//////////////////////////      draw(): runs until the application stops. can be throttleded down by the frameRate manually
//////////////////////////      or because everything gets too heavy
    
	void draw();

//////////////////////////      it would be possible to use lighting, but, yeah, no
    
// ofLight light;
    
//////////////////////////      wheeew our mathematical meshes. so nice, so nice.
    
    Parabala parabala;
    Torus torus;
    Mobius mobius;
    Tube tube;
    Spiral spriral;
    Thing thing;
    Steiner steiner;
    Boys boys;
    Breather breather;
    
//////////////////////////      mesh: a mesh
    
    ofVboMesh mesh;
    
//////////////////////////      preview of how it will be done
	ofTexture * colormap;
    ofTexture * bumpmap;
    
    vector<ofImage> images;
    vector<ofImage> pineapples;
    
//////////////////////////      shader: calculates on the graphic card. good move, jakob.
    
	ofShader shader;
    
/////super//comment///////      more on shaders: http://openframeworks.cc/tutorials/graphics/shaders.html
/////super//comment///////      more on shaders: https://www.shadertoy.com/
    
//////////////////////////      vidGrabber: the webcam. maaaaaaaybe we want to use it. maaaaaaaybe. actually definitely.
    
	ofVideoGrabber vidGrabber;
    
/////super//comment///////      fbos, our little "virtual screens"
/////super//comment///////      more on fbos: http://openframeworks.cc/documentation/gl/ofFbo.html
/////super//comment///////      more on fbos: http://www.opengl.org/wiki/Framebuffer_Object
    
//////////////////////////      fbo: great name. this is our main-fbo, which we draw everything in.
//////////////////////////      why not drawing directly on the screen? uuuh good question, but a little tricky. I'll explain later
    
    ofFbo mainFbo;
    
//////////////////////////      texFbo: the fbo where we can draw in to use as a texture.. hmmm ... to distort meshes maybe?
    
    ofFbo texFbo;
    
//////////////////////////      squirrelFbo: i think this is the fbo we draw our meshes in. quite sure.
    
    ofFbo squirrelFbo;
    
    
//////////////////////////      CONTROL
    
    float shaderMaxHeight;      // 0-100
    float shaderDisplace;       // 1-0
    
    
};

//..............................#endif: no-worry-yet stuff
#endif

